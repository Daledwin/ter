# Typer Python

Etude de l'influence du typage static sur python et sur le code de sa bibliothèque standard. 
Nous verrons ensemble l'outil mypy qui permet le typage static par certainnes annotation, mais qui contient aussi un puissant moteur d'inférence.

## Inference de Type

L'inference de type est le fait de deviner en vu du contexte le type d'une variable.

Par exemple ici de deviner le type de x:

```py
x = 10
# x : bultins.int
```

```py
class NewG(Generic[T]):
    def __init__(self, content: T) -> None:
        self.content = content

NewG(1)  # infere le type de NewG[int]
NewG[int](1)  # precise le type de NewG[int]
st: str = 'some string'
NewG[int](s)  # Type error
```
Ici encore une simple resolutuion de l'expression nous permet une comparaison de type.

D'autre cas sont néanmoins plus complexe: 

### Recherche d'un type commun:

```py
class Animals(object):
    def __init__(self):
        pass
    
class Rhino(Animals):
     def __init__(self):
        super().__init__()
    
class Elephant(Animals):
     def __init__(self):
        super().__init__()
    
class Snake(Animals):
     def __init__(self):
        super().__init__()


test = [Rhino(), Elephant(), Snake()] # builtins.list[multipleExpression.Animals*]
mypy: list[Animals] = [Rhino(), Elephant(), Snake()]

```
Ici l'inférence revelle la présence d'une liste d'annimaux, alors que si l'on appliquer le même principe d'inférence que plus haut, nous obetenions : builtins.list[multipleExpression.Rhino, multipleExpression.Elephant, multipleExpression.Snake] 

### Contextual Typing ```/!\ error```

```py
from typing import Iterator
from typing import Optional

class Animals(object):
    def __init__(self): pass

class Baby(Animals):
    def __init__(self, age: int):
        self.age = age
        super().__init__()
       
    def age(self, i: int) -> int:
        self.age = i
        return self.age

class Rhino(Animals):
     def __init__(self, baby: Optional[Baby]):
         self.baby = baby
         super().__init__()

     def baby (self, baby: Baby) -> Baby :
        return self.baby

baby = Baby(4)  
age = baby.age(3)
test = Rhino(baby).baby().age(5)
mypy: int = Rhino(baby).baby().age(5)

```
Ici le type de chaque élement est calculé par l'élement appelant, Baby de Rhino et int de Rhino.

***exemples tirés de [tyscript](https://www.typescriptlang.org/docs/handbook/type-inference.html) module de typage en javascript***


## Liste des Type et Inférence par type

### Any
```py
from typing import Any

test = None # test: <partial None>
mypy: Any = None

```

### Str

```py
test = 'test' # test: builtins.str
mypy: str = 'test'

```

### Int

```py
test = 10 # test: builtins.int
mypy: int = 10 

```

### Float

```py
test = 1.2 # test: builtins.float
mypy: float = 1.2 

```

### Bool

```py
test = True # test: builtins.bool
mypy: bool = True 

```

### Bytes

```py
test = b'\xf0\xf1\xf2' # test: builtins.bytes
mypy: bytes = b'\xf0\xf1\xf2' 

```

### Bytearray

```py
test = bytearray(b'\xf0\xf1\xf2') # test: builtins.bytearray
mypy: bytearray = bytearray(b'\xf0\xf1\xf2') 

```

### Complex

```py
test  = complex('1+2j') # test: builtins.complex
mypy: complex = complex('1+2j') 

```

### Tuple

```py
test = (1,2) # test: builtins.tuple
mypy: tuple = (1,2) 

```

### List

```py
test = [1] # test: builtins.list[bultins.int*]
mypy: list[int] = [1] 

```

### Iterator

```py
from typing import Iterator

mytuple = ("apple", "banana", "cherry") # mytuple: Tuple[builtins.str, builtins.str, builtins.str]

test = iter(mytuple) # test: typing.Iterator[builtins.str*]
mypy: Iterator = iter(mytuple)

```

### Class

```py
class A:
    def __init__(self):
        pass

test = A() # test: class.A
mypy: A = A() 

```
### Dict

```py
test = {'one': 1, 'two': 2, 'three': 3} # test: builtins.dict
mypy: dict[str, int] = {'one': 1, 'two': 2, 'three': 3} 

```
### Callable/Lambda

```py
from typing import Callable

test = lambda x: x*2 # test: def (x: Any) -> Any
mypy: Callable = lambda x: x*2

```
### Range

```py
test = range(10) # test: builtins.range
mypy: range = range(10) 

```
### Set

```py
test = set('ab') # test: builtins.set[builtins.str*]
mypy: set = set('ab') 

```

### Frozenset

```py
test = frozenset('ab') # test: builtins.frozenset[builtins.str*]
mypy: frozenset = frozenset('ab') 

```
### Exception

```py
test = Exception() # test: builtins.Exception
mypy: Exception = Exception() 

```

### File

```py
from typing import IO

test = open("int.py", "r") # test: builtins.TextIO
mypy: IO = open("int.py", "r") 

```

### Memoryview

```py
test = memoryview(b'abcefg') # test: builtins.memoryview
mypy: memoryview = memoryview(b'abcefg')

```

### NotImplemented

```py
test = NotImplemented # test: builtins._NotImplementedType
mypy: int = NotImplemented
# NotImplemented correspond à n'importe quel environment ici int aurais pu etre remplacer par n'importe quel autre type

```








