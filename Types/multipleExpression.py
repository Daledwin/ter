class Animals(object):
    def __init__(self):
        pass
    
class Rhino(Animals):
     def __init__(self):
        super().__init__()
    
class Elephant(Animals):
     def __init__(self):
        super().__init__()
    
class Snake(Animals):
     def __init__(self):
        super().__init__()


test = [Rhino(), Elephant(), Snake()]
mypy: list[Animals] = [Rhino(), Elephant(), Snake()]

reveal_locals()