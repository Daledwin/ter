from typing import Iterator
from typing import Optional

class Animals(object):
    def __init__(self): pass

class Baby(Animals):
    def __init__(self):
        super().__init__()
       
    def age(self, i: int) -> int:
        return i

class Rhino(Animals):
     def __init__(self):
          super().__init__()

     def baby (self, baby: Baby) -> Baby :
        return baby

baby = Baby()  
age = baby.age(4)
test = Rhino().baby(baby).age(4)
mypy: int = Rhino().baby(baby).age(4)

reveal_locals()