from typing import Callable

test = lambda x: x*2
mypy: Callable = lambda x: x*2

reveal_locals()