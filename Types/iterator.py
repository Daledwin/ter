from typing import Iterator

mytuple = ("apple", "banana", "cherry")

test = iter(mytuple)
mypy: Iterator = iter(mytuple)

reveal_locals()